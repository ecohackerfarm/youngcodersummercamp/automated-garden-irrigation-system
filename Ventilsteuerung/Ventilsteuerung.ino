// This program controlls the valves in the garden
//
//
int LightThreshhold = 500;
// Setting the delays for the different valves
int delays[4] = {15000,
                 15000,
                 15000,
                 15000
                };
int pumppin = 5;
int numdelays = 4
// The delay for checking the brightness
int checkdelay = 10000;
// ---------------------------------------------------------------------------------------------------------
int brightness;
int valveindex = 0;
int pins[4] = {1, 2, 3, 4};
int lightsensorpin = A0;
int del;


void setup() {
  Serial.begin(9600);
  for (int pin = 0; pin < numdelays; pin++) {
    pinMode(pins[pin], OUTPUT);
  }
}

void loop() {
  brightness = analogRead(lightsensorpin);
  Serial.println(brightness);
  if  (brightness < LightThreshhold) {
    digitalWrite(pumppin, HIGH)
    valveindex = 0;
    while (valveindex < numdelays){
      Serial.println(valveindex);
      del = delays[valveindex];
      digitalWrite(pins[valveindex], HIGH);
      delay(del);
      digitalWrite(pins[valveindex], LOW);
      valveindex ++;
    digitalWrite(pumppin, LOW)
    }
  }
  delay(checkdelay);
}
