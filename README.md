# Automated garden irrigation system

An Automated garden irrigation system with arduino.
List of equipment:
- Arduino Nano
- light sensitive resistor 2kΩ - 6kΩ
- resistor 2kΩ
- relay/s
- valve/s 12V
- power supply 12V



For a breadbord, schematic or circuit board view open fritzing sketch with fritzing: https://fritzing.org/download/
for more information visit https://wiki.ecohackerfarm.org/research:automated_irrigation_system
